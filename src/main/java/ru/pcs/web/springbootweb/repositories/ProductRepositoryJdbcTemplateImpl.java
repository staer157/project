package ru.pcs.web.springbootweb.repositories;

import ru.pcs.web.springbootweb.model.Product;
import ru.pcs.web.springbootweb.model.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component //  данной анотацией помещаем классы, на основе Spring создаст " бин(компонент)" и поместит его в контейнер
public class ProductRepositoryJdbcTemplateImpl implements ProductRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into product(description, price, quantity, category) values(?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from product order by id";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = ?;";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = ?";
    //language=SQL
    private static final String SQL_SELECT_BY_CATEGORY = "select * from product where category = ?";

    private static final String SQL_UPDATE = "update product set description = ?, price = ?, quantity = ?, category = ? where id = ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> goodsRowMapper = (row, rowNumber) -> {
        Product product = new Product();
        product.setId(row.getInt("id")); // адишник
        product.setDescription(row.getString("description")); // описание
        product.setPrice(row.getDouble("price")); //цена
        product.setQuantity(row.getInt("quantity")); // доступный
        product.setCategory(ProductCategory.fromId(row.getInt("category")));
        return product;
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, goodsRowMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getQuantity(), product.getCategory().getId());
    }

    @Override
    public void delete(Integer productId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, productId);
    }

    @Override
    public Product findById(Integer productId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, goodsRowMapper, productId);
    }

    @Override
    public List<Product> findByCategory(int category) {
        return jdbcTemplate.query(SQL_SELECT_BY_CATEGORY, goodsRowMapper, category);
    }

    @Override
    public void update(Product product) {
        jdbcTemplate.update(SQL_UPDATE, product.getDescription(), product.getPrice(), product.getQuantity(), product.getCategory().getId(),product.getId());
    }
}



