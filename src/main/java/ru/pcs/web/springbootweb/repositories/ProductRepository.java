package ru.pcs.web.springbootweb.repositories;

import ru.pcs.web.springbootweb.model.Product;

import java.util.List;

public interface ProductRepository {

    List<Product> findAll(); // на вход принемается

    void save(Product goods);

    void delete(Integer productId);

    Product findById(Integer productId);

    List<Product> findByCategory(int category);
    void update(Product goods);
}
