package ru.pcs.web.springbootweb.forms;
import lombok.Data;

@Data
public class ProductForm { //в продуктах должна содержать такие поля

    private Integer category;
    private String description; // описание товара
    private Double price; // цена в рублях
    private Integer quantity; // количество

}
