package ru.pcs.web.springbootweb.services;
import ru.pcs.web.springbootweb.forms.ProductForm;
import ru.pcs.web.springbootweb.model.Product;
import java.util.List;
public interface ProductService {
    void addProduct(ProductForm form); // метод
    List<Product> getAllProduct(); // на вход принемается
    default void deleteProduct(Integer productId) {}

    Product getProduct(Integer productId);

    List<Product> getProductByCategory(int category);
    void updateProduct(ProductForm form, Integer productId);

}

