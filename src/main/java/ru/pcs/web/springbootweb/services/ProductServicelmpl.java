package ru.pcs.web.springbootweb.services;

import ru.pcs.web.springbootweb.forms.ProductForm;
import lombok.RequiredArgsConstructor;
import ru.pcs.web.springbootweb.model.Product;
import ru.pcs.web.springbootweb.model.ProductCategory;
import org.springframework.stereotype.Component;
import ru.pcs.web.springbootweb.repositories.ProductRepository;


import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductServicelmpl implements ProductService {
    private final ProductRepository productRepository;
    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .category(ProductCategory.fromId(form.getCategory()))
                .build();

        productRepository.save(product);
    }

    @Override
    public List<Product> getAllProduct() {
        return (List<Product>)productRepository.findAll();
    }
    @Override
    public void deleteProduct(Integer productId) {

        productRepository.delete(productId);
    }
    @Override
    public Product getProduct(Integer productId) {
       return productRepository.findById(productId);
    }

    @Override
    public List<Product> getProductByCategory(int category){
        return productRepository.findByCategory(category);
    }

    @Override
    public void updateProduct(ProductForm form, Integer productId) {
        Product product = productRepository.findById(productId);
        product.setDescription(form.getDescription());
        product.setPrice(form.getPrice());
        product.setQuantity(form.getQuantity());
        product.setCategory(ProductCategory.fromId(form.getCategory()));
        productRepository.update(product);
    }
}
