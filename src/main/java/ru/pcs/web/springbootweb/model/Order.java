package ru.pcs.web.springbootweb. model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //
@AllArgsConstructor // есть конструктор со всеми параметрами
@NoArgsConstructor // есть конструктор вообще без параметров
@Builder //она позволяет создавать объекты немного по другому

public class Order {
    private Integer quantity; // количество
    private Integer id; //
    private Product product_id; //сылка на позицию в Id к таблице product
    private User owner_id; //ссылка на позичию а таблице user
}
