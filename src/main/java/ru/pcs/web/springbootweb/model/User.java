package ru.pcs.web.springbootweb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //
@AllArgsConstructor // есть конструктор со всеми параметрами
@NoArgsConstructor // есть конструктор вообще без параметров
@Builder
public class User {

    private Integer id; //индекс
    private String name; //имя
    private String surname; // фамилия

}
