package ru.pcs.web.springbootweb.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

// анатациии, которые работают в автономном режиме
@Data //
@AllArgsConstructor // есть конструктор со всеми параметрами
@NoArgsConstructor // есть конструктор вообще без параметров
@Builder //она позволяет создавать объекты немного по другому
public class Product { //продукт

    private ProductCategory category; // продуктовая категория (внутренняя строка справочника)
    private Integer id; //индекс
    private String description; // описание товара
    private Double price; // цена в рублях
    private Integer quantity; // количество товара

}