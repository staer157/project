package ru.pcs.web.springbootweb.model;

public enum ProductCategory {
    Shoes(1, "Обувь"),
    Dress(2, "Одежда"),
    Equipment(3,"Снаряжение"),
    SportsNutrition(4, "Спортивное питание");
    int id;
    String description;
    private ProductCategory(int id, String description){ //конструктор
        this.id = id;
        this.description = description;
    }
    public int getId(){
        return id;
    }
    public String getDescription(){
        return description;
    }
    public static ProductCategory fromId(int id){
        for (ProductCategory p : values()){
            if (p.getId() == id){
                return p;
            }
        }
        return null;
    }
}
