package ru.pcs.web.springbootweb.controller;

import ru.pcs.web.springbootweb.forms.ProductForm;
import ru.pcs.web.springbootweb.model.Product;
import ru.pcs.web.springbootweb.model.ProductCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pcs.web.springbootweb.services.ProductService;

import java.util.Arrays;
import java.util.List;

@Controller //помечаем классы-контроллеры, это классы которые способны обработать HTTP классы
public class ProductController {

    private final ru.pcs.web.springbootweb.services.ProductService productService;

    @Autowired // можно повесть на конструкьтор или поле. Находит нужный по типу и бин делает его inject
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping("/")
    public String schowFirstViem(Model model) {
        List<Product> products = productService.getAllProduct();
        List<ProductCategory> categories = Arrays.asList(ProductCategory.values());
        model.addAttribute("products", products); //продукты
        model.addAttribute("categories", categories); // категории
        return "products";
    }

    @GetMapping("/product") // получаем пользователей
    public String getCatalogPage(Model model) {
        List<Product> products = productService.getAllProduct();
        List<ProductCategory> categories = Arrays.asList(ProductCategory.values());
        model.addAttribute("products", products); //продукты
        model.addAttribute("categories", categories); // категории
        return "products";
    }

     @GetMapping("/product/add") // получить имя конретного пользователя на второй странице
  public String getProductAddPage(Model model) {
         model.addAttribute("categories", Arrays.asList(ProductCategory.values())); // категории
         return "product_add";
     }

    @GetMapping("/product/{product-id}") // все продукты какого то пользователя
    public String getCatalogPage(Model model, @PathVariable("product-id") Integer productId) {
        Product product = productService.getProduct(productId);
        List<ProductCategory> categories = Arrays.asList(ProductCategory.values());
        model.addAttribute("product", product);
        model.addAttribute("categories", categories); // категории
        return "product";
    }

    @PostMapping("/product/add") //обработка Get запроса
    public String addProducts(ProductForm Form) {
        productService.addProduct(Form);
        return "redirect:/product";
    }

    @GetMapping("/product/{product-id}/delete")
    public String deleteProducts(@PathVariable("product-id") Integer productId) {
        productService.deleteProduct(productId);
        return "redirect:/product";
    }

    @PostMapping("/product/{product-id}/update")
    public String update(@PathVariable("product-id") Integer productId, ProductForm form) {
        productService.updateProduct(form, productId);
        return "redirect:/product";
    }

    @PostMapping("/product/search")
    public String showProductByCategory(Model model, @RequestParam("category") Integer categoryId) {
        List<Product> products = productService.getProductByCategory(categoryId);
        List<ProductCategory> categories = Arrays.asList(ProductCategory.values());
        model.addAttribute("products", products); //продукты
        model.addAttribute("categories", categories); // категории
        return "products";
    }
}