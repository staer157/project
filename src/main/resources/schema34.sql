    create table product -- Создаем таблицу с именем ТОВАР(product)
    (
        id serial primary key,  -- это индифекатор строки, всегда уникальный и генерируется базой данных
        -- Определяем столбцы
        description varchar(500),-- описание товара
        price varchar(6000), --цена в рублях
        quantity integer check ( quantity > 0 and quantity < 1000) --(количество) integer (целое число) водятся числовые ограничения
    );
    -- описание товара и количество товара
    insert into product(id, description, price, quantity) values (1, 'Кроссовки', 5000, 100);
    insert into product(id, description, price, quantity) values (2, 'Гири', 3000, 30);
    insert into product(id, description, price, quantity) values (3, 'Блины', 1500, 80);
    insert into product(id, description, price, quantity) values (4, 'Штанги', 2000, 50);
    insert into product(id, description, price, quantity) values (5, 'Гантели', 1000, 66);
    insert into product(id, description, price, quantity) values (6, 'Мяч', 500, 156);
    insert into product(id, description, price, quantity) values (7, 'Прыгалки', 300, 150);
    insert into product(id, description, price, quantity) values (8, 'Перчатки', 400, 60);
    insert into product(id, description, price, quantity) values (9, 'Лыжи',4500, 18);


create table customer --создаем таблицу Покупателей (customer)
(
    id serial primary key, -- индифнкатор
    name varchar(1000), -- Имя
    surname  varchar(1000) -- Фамилия
);
-- Данные про заказчикам

insert into customer(id, name, surname) values (1,'Александр', 'Самсонов');
insert into customer(id, name, surname) values (2, 'Алена', 'Коршунова');
insert into customer(id, name, surname) values (3, 'Вадим', 'Мищенко');
insert into customer(id, name, surname) values (4, 'Михаил', 'Сафронов');
insert into customer(id, name, surname) values (5, 'Дмитрий', 'Самсонов');
insert into customer(id, name, surname) values (6, 'Вася', 'Пупкин');

create table "order" --создаем таблицу заказа (order)
(
    id serial primary key,
    order_date varchar(1000), -- дата заказа
    quantity   varchar(1000), -- количество товара
    owner_id   integer,       -- (внешний ключ ссылка на строку из другой таблицы)
    foreign key (owner_id) references customer (id)
);

--Заказы

insert into "order"(order_date, quantity, owner_id) values ('24.11.21', 'Гири 2 штуки, Кросовки 5 пар, Мяч 3шт, Штанги 2 шт', 1);
insert into "order"(order_date, quantity, owner_id) values ('11.06.21', 'Блины 10 шт', 2);
insert into "order"(order_date, quantity, owner_id) values ('28.03.21', 'Лыжи 1 пару, Гирю 1 шт, Прыгалки 4 шт', 3);
insert into "order"(order_date, quantity, owner_id) values ('01.09.21', 'Гантелей 10 шт, Перчаток 6 пар', 4);
insert into "order"(order_date, quantity, owner_id) values ('06.08.21', 'Лыжи 3 пар', 5);
insert into "order"(order_date, quantity, owner_id) values ('03.10.21', 'Кроссовки 10 пар, Гири 10 шт, Блины 20 шт, Штанги 5 шт, Гантели 10 шт', 6);
insert into "order"(order_date, quantity, owner_id) values ('11.06.21', 'Блины 10 шт', 1);

